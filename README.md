# hitatek-task

### Installing dependencies

<br>

```
cd /<path-to-folder>/hitatek-task
npm install

```

<br>

### Running Client and Server

```
cd /<path-to-folder>/hitatek-task
npm start

```
