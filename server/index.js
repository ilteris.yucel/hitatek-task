const express = require('express');
const path = require('path');
const app = express();
const cors = require('cors');
const fs = require('fs');
const { json } = require('express');
const port = 5000;

app.use(express.urlencoded());
app.use(express.json());
app.use(cors({
  origin: '*'
}));


app.get('/get-all-students',(req, res)  => {
   const rawData = fs.readFileSync('./data/data.json');
   const jsonData = JSON.parse(rawData);
   res.json(jsonData);
});

app.get('/get-a-student/:studentId', (req, res) => {
   const studentId = req.params.studentId;
   const rawData = fs.readFileSync('./data/data.json');
   const jsonData = JSON.parse(rawData);
   const student = jsonData.find(s => s.id === studentId);
   if(!student){
      res.sendStatus(404);
   }
   res.json(student);
});

app.put('/add-a-student', (req, res) => {
   const rawData = fs.readFileSync('./data/data.json');
   const jsonData = JSON.parse(rawData);
   const student = {
      "id": req.body.id,
      "name": req.body.name,
      "surname": req.body.surname,
      "notes": req.body.notes ? req.body.notes : "",
      "avg": req.body.notes ? req.body.notes.split(',').reduce((a, b) => a + b, 0) / req.body.notes.length : 0
   };
   jsonData.push(student);
   fs.writeFileSync('./data/data.json', JSON.stringify(jsonData, null, 4));
   res.sendStatus(200);
});
//Material UI DataGrid ücretsiz sürümünde apiRef'i desteklemediği için
//UI ile bağlanamamıştır.
app.patch('/update-score-list/:studentId', (req, res) => {
   const studentId = req.params.studentId;
   const rawData = fs.readFileSync('./data/data.json');
   const jsonData = JSON.parse(rawData);
   const studentIndex = jsonData.findIndex(s => s.id === studentId);
   if(studentIndex === -1){
      res.sendStatus(404);
   }
   jsonData[studentIndex].notes = req.body.notes;
   jsonData[studentIndex].avg = req.body.notes.split(',').reduce((a, b) => a + b, 0) / req.body.notes.length;
   fs.writeFileSync('./data/data.json', JSON.stringify(jsonData, null, 4));
   res.sendStatus(200);
});
//Material UI DataGrid ücretsiz sürümünde apiRef'i desteklemediği için
//UI ile bağlanamamıştır.
app.delete('/delete-students', (req, res) => {
   const rawData = fs.readFileSync('./data/data.json');
   const jsonData = JSON.parse(rawData);
   const deleteList = req.body.deleteList;
   jsonData = jsonData.filter((student) => {
      ret = true;
      if(deleteList.include(student['id'])){
         ret = false;
      }
      return ret;
   })
})

app.listen(process.env.PORT || port, () => {
   console.log(`App listening on port ${port}!`)
 });
 
 

app.use("/data", express.static(path.join(__dirname, 'data')));
