const validateNotes = (notes:string):boolean => {
    const re = /^\d+(?:[ \t]*,[ \t]*\d+)?$/;
    return re.test(notes);
}

const calculateAvg = (notes:string):number => {
    const numberList:Array<string> = notes.split(',');
    const avg = numberList.map((i) => parseInt(i)).reduce((a, b) => a + b, 0) / numberList.length;
    const retVal = avg ? avg : 0;
    return retVal;
}

const randomInt = (begin:number, end:number):number => {
    return Math.floor((Math.random() * end) + begin);
}

const makeId = (length:number):string => {
    let result           = '';
    const characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    let charactersLength = characters.length;
    for ( var i = 0; i < length; i++ ) {
        result += characters.charAt(Math.floor(Math.random() * 
        charactersLength));
   }
   return result;
}

export { validateNotes, calculateAvg, randomInt, makeId };