import React from "react";
import { DataGrid,  GridCellValue, GridColDef, GridValueGetterParams } from '@mui/x-data-grid';
import Button from '@mui/material/Button';
import ButtonGroup from '@mui/material/ButtonGroup';
import { validateNotes, calculateAvg, randomInt, makeId } from './utils'

function App() {
  const columns: GridColDef[] = [
    { field: 'id', headerName: 'ID', width: 70 },
    { field: 'name', headerName: 'FIRST NAME', width: 130 },
    { field: 'surname', headerName: 'LAST NAME', width: 130 },
    { field: 'notes', headerName: 'NOTES', description: "Insert notes as comma seperated list" ,editable: true,  
    preProcessEditCellProps: (params) =>  { 
      const isValid = validateNotes(String(params.props.value));
      return { ...params.props, error: !isValid };
    }
    , width: 200 },
    { field: 'avg', headerName: 'AVG', type: 'number', valueGetter : (params: GridValueGetterParams):GridCellValue => { return calculateAvg(String(params.getValue(params.id, 'notes')));}, width: 130 }
  ];
  const [hasError, setError] = React.useState<boolean>(false);
  const [students, setStudents] = React.useState<Array<Object>>([]);

  React.useEffect(() => {
    async function fetchData(){
      const res = await fetch('http://localhost:5000/get-all-students');
      res.json()
         .then(res => {setStudents(res)})
         .catch(err => setError(err))
    }
    fetchData();
    return () => {
      setStudents([]);
      setError(false);
    };
  }, []);
  let content = hasError ? <p>Could not fetched data.</p> : <DataGrid rows={students} columns={columns} pageSize={5} rowsPerPageOptions={[5]} checkboxSelection />;
  return (
    <div style={{ height: 400, width: '100%' }}>
      {content}
      <ButtonGroup>
      <Button onClick={() => {
        const s = {
          id: '' + randomInt(1, 1000),
          name: makeId(7),
          surname: makeId(5),
          notes: randomInt(0, 100) + ',' + randomInt(0,100),
          avg:null
        }
        const newList = [...students, s];
        setStudents(newList);
        fetch('http://localhost:5000/add-a-student', {
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
          },
          method: 'PUT',                                                              
          body: JSON.stringify( s )           
        })
      }} variant="contained">ADD RANDOM STUDENT</Button>
      </ButtonGroup>
      <Button>REMOVE SELECTED STUDENT</Button>
    </div>
  );
}

export default App;
